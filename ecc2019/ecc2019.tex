\documentclass[serif]{beamer}

\usepackage{amsmath}
\usepackage{beamerthementnuenglish}
\usefonttheme{professionalfonts}
%\usefonttheme{serif}
%\usefonttheme{serif}     % Font theme: serif
%\usepackage{ccfonts}     % Font family: Concrete Math
%\usepackage[T1]{fontenc} % Font encoding: T1

%\usepackage{pgfpages}
%\setbeameroption{show notes on second screen=right}
\usepackage{txfonts}
\usepackage[T1]{fontenc}

\setbeamerfont{frametitle}{size=\large}

\usepackage[utf8]{inputenc}
\usepackage{mathtools}
\usepackage{amssymb}

\usepackage[many]{tcolorbox}

\usepackage{tikz}

\usetikzlibrary{shadows,arrows,positioning,fit,calc,shapes}
% Define the layers to draw the diagram
\pgfdeclarelayer{background}
\pgfdeclarelayer{foreground}
\pgfsetlayers{background,main,foreground}

% Define block styles  
\tikzstyle{materia}=[draw, fill=black!20, text width=6.0em, text centered,
minimum height=1em]
\tikzstyle{module} = [materia, text width=9em, minimum width=10em,
minimum height=1em]
\tikzstyle{texto} = [above, text width=6em, text centered]
\tikzstyle{linepart} = [draw, thick, color=black!50, -latex', dashed]
\tikzstyle{line} = [draw, thick, color=black, -latex']
\tikzstyle{ur}=[draw, text centered, minimum height=0.01em]
\tikzstyle{edge} = [draw, thick, color=black]

\tikzstyle{backgroundbox} = [draw,fill=black!10, text width=8em, minimum height=3em,drop shadow,rounded corners,inner sep=7mm, text depth=2cm]

% Define distances for bordering
\newcommand{\blockdist}{1.3}
\newcommand{\edgedist}{1.5}
\newcommand{\nchi}{\raisebox{1.7pt}{$\chi$}}
\newcommand{\module}[3]{node (#1)[module]
  {#2\\{\scriptsize\textit{#3}}}}

\usepackage[absolute,overlay]{textpos}
\usepackage{accents}
\MakeRobust{\underaccent}
\usepackage{graphics}

\newcommand{\ubar}[1]{\underaccent{\bar}{#1}}

\title{GNSS-antenna lever arm compensation in aided inertial navigation of UAVs}
\author{Bård Nagy Stovner}
\institute{Department of Engineering Cybernetics\\Norwegian University of Science and Technology}
\date{2019}

\usepackage{graphicx}
\usepackage{epstopdf}

\usepackage{tikz}
\usetikzlibrary{shapes.symbols,patterns}

\usepackage{enumitem}
\setlist[itemize,1]{label=$\bullet$}
\setlist[itemize,2]{label=$-$}

\begin{document}
\frame{\titlepage}

\begin{frame}
  \frametitle{Notation}
 \begin{textblock*}{11cm}(1cm,2cm)
   \begin{tcolorbox}[colframe=red!80!black]
     \begin{minipage}{.3\linewidth}
       \huge{$x^a_{bc}$}
       
       \vspace{8pt}
       \huge{$R^d_e$}
     \end{minipage}%
     \begin{minipage}{.7\linewidth}
       \Large{Vector from $b$ to $c$ in frame $a$}

       \vspace{16pt}
       \Large{Rotation from frame $e$ to frame $d$} 
     \end{minipage}
   \end{tcolorbox}
 \end{textblock*} 
 \vspace{2cm}
 \begin{minipage}{.4\linewidth}
 \begin{itemize}
 \item[\Large{$p^n_{nb}$}] \Large{Position}
 \item[\Large{$R^n_b$}] \Large{Rotation matrix}
 \end{itemize}
 \end{minipage}%
 \begin{minipage}{.6\linewidth}
 \begin{itemize}
 \item[\Large{$\hat{p}^n_{nb}$}] \Large{Position estimate}
 \item[\Large{$R^n_{\hat{b}}$}] \Large{Rotation matrix estimate}
 \end{itemize}
 \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{Problem Statement}
  Integrate
  \begin{itemize}
  \item IMU
    \begin{itemize}
    \item Accelerometer
    \item Angular rate sensor
    \end{itemize}
  \item 1, 2, or 3 GNSS-receivers
  \end{itemize}
  \pause
  to produce estimates of
  \begin{itemize}
  \item position 
  \item velocity
  \item attitude
  \item<5-> {\color{red}antenna positions}
  \end{itemize}
  using extended Kalman filter (EKF)
  \begin{textblock*}{7cm}(5.5cm,0.5cm)
  \onslide<4->{
  \begin{tcolorbox}[colframe=red!80!black]
    Unknown $p^b_{ba_i}\; i=1,2,3$
  \end{tcolorbox}
  }
  \end{textblock*}
  \begin{textblock*}{7cm}(5.5cm,2cm)
  \onslide<6->{
  \begin{tcolorbox}[colframe=green!70!black]
    Known $\|p^b_{ba_i}\|$, $\|p^b_{a_ja_i}\|\; i,j=1,2,3$
  \end{tcolorbox}
  }
  \end{textblock*}
  \begin{textblock*}{6cm}(6.5cm,3.5cm)
    \begin{tikzpicture}
     \node[inner sep=0pt] (uav) at (0,0) {\includegraphics[width=\textwidth]{x8.jpg}};
     
     \node[circle,fill=blue!80!black] (imu) at (0.65,0.2){}; 
     \node[circle,fill=red] (a1) at (1.5,-.3){}; 
     \node[circle,fill=red] (a2) at (-1.5,0){}; 
     \node[circle,fill=red] (a3) at (2.2,0.35){}; 

     \path [edge, color=blue!80!black] ($(imu)+(-1,1)$) -- node [above,pos=0,color=black] {IMU} (imu);
     \path [edge, color=red!80!black] ($(a1)+(0,-1)$) -- node [below,pos=0,color=black] {GNSS receivers} (a1);
     \path [edge, color=red!80!black] ($(a1)+(0,-1)$) -- (a2);
     \path [edge, color=red!80!black] ($(a1)+(0,-1)$) -- (a3);
   \end{tikzpicture} 
 \end{textblock*}
\end{frame}

\begin{frame}
  \frametitle{State of the Art}
  $N$ receivers measure
    \begin{align*}
      p^n_{na_i} = p^n_{nb} + R^n_b\color{red}p^b_{ba_i}\color{black}
    \end{align*}
    Add $p^b_{ba_i}$ to state vector
    \begin{align*}
      x =
      \begin{bmatrix}
        p^n_{nb}{}^{\top} &
        v^n_{nb}{}^{\top}&
        q^n_b{}^{\top}&
        b^b_{acc}{}^{\top}&
        b^b_{ars}{}^{\top}&
        \color{red}p^b_{ba1}\color{black}{}^{\top}&
        \hdots&
        \color{red}p^b_{baN}\color{black}{}^{\top}&
      \end{bmatrix}
    \end{align*}
  \onslide<3->{
  \hrule
  \begin{minipage}{.5\linewidth}
    \begin{align*}
      p^n_{na_i} = p^n_{nb} + R^n_b \color{red}l_1^b\color{black} +R^n_b\color{red}R^b_a\color{black}l^a_{i1}
    \end{align*}
    Define an antenna-frame $\{a\}$ and estimate
    \begin{itemize}
    \item its origin $l^b_1$
    \item rotation to body-fixed frame $R^b_a$
    \end{itemize}
\end{minipage}%
 \begin{minipage}{.55\linewidth}
   %\vspace{-1.5cm}
      \begin{figure}[t]
      \centering
      \includegraphics[width=.8\textwidth]{hong-antennaframe.png}
      \end{figure}
  \end{minipage}}
    \begin{textblock*}{3cm}(4.5cm,6.8cm)
      \onslide<4->{
      \begin{tcolorbox}[colframe=red!80!black]
      6 variables!
      \end{tcolorbox}}
    \end{textblock*}
    \begin{textblock*}{3.5cm}(8.5cm,1cm)
      \onslide<2->{
      \begin{tcolorbox}[colframe=red!80!black]
      $3N$ variables!
      \end{tcolorbox}}
    \end{textblock*}
\end{frame}

\begin{frame}
  \frametitle{State of the art (antenna frame formulation)\footnotemark[1]}
  \begin{minipage}{.55\linewidth}
    \begin{itemize}
    \item Requires 3 antennas
      \begin{itemize}
      \item 1 - origo of antenna frame
      \item 2 - to define $\vec{x}$ and $\vec{y}$ axes
      \end{itemize}
    \item Represents lever arms by
      \begin{itemize}
      \item 3 translations $l_1^b$
      \item 3 rotations $R^b_a$
      \end{itemize}
    \end{itemize}
 \end{minipage}%
 \begin{minipage}{.45\linewidth}
   \vspace{-1.5cm}
      \begin{figure}[t]
      \centering
      \includegraphics[width=.8\textwidth]{hong-antennaframe.png}
      \end{figure}
  \end{minipage}

  \footnotetext[1]{\emph{Observability analysis of INS with a GPS multi-antenna system}, Sinpyo Hong et al., KSME International Journal 2002}
\end{frame}

\begin{frame}
  \frametitle{Our solution for $N\geq2$}
  \vspace{-20pt}
  \begin{itemize}
  \item Measure distances by hand
  \item Define an antenna-frame $\{a\}$\\ with IMU as origin!
  \item $R^b_a \Rightarrow$ 3 variables for any $N$
  \item Strategy
    \begin{itemize}
    \item $\vec{x}_a$-axis points to antenna 1
    \item $\vec{y}_a$-axis projects antenna 2 onto $\vec{x}_a$-axis
    \item $\vec{z}_a$-axis perpendicular to $\vec{x}_a$ and $\vec{y}_a$
    \end{itemize}
  \end{itemize}
  \begin{textblock*}{12cm}(.2cm,6.1cm)
    \onslide<2->{
    \begin{align*}
      p^a_{ba_1} =
      \begin{bmatrix}
        x_1\\0\\0
      \end{bmatrix}=
      \begin{bmatrix}
        L_1\\0\\0
      \end{bmatrix},
      p^a_{ba_2} = 
      \begin{bmatrix}
        x_2\\y_2\\0
      \end{bmatrix}=
      \begin{bmatrix}
        \frac{L_1^2 + L_2^2 - L_{12}^2}{2x_1}\\\sqrt{L_2^2 - x_2^2}\\0
      \end{bmatrix},
      p^a_{ba_3} = 
      \begin{bmatrix}
        x_3\\y_3\\z_3
      \end{bmatrix}=
      \begin{bmatrix}
        \frac{L_1^2 + L_3^2 - L_{13}^2}{2x_1}\\
        \frac{L_2^2 + L_3^2 - L_{23}^2-x_2x_3}{2y_2}\\
        s_i\sqrt{L_3^2 - x_3^2 - y_3^2}
      \end{bmatrix}
    \end{align*}
    }
  \end{textblock*}
  \begin{textblock*}{6.5cm}(6cm,.5cm)
    \resizebox{\linewidth}{!}{%
      \begin{tikzpicture}
      \coordinate (O) at (0,0);
  
      \coordinate (flu) at (-.4,.4);
      \coordinate (fru) at (0,.4);
      \coordinate (fld) at (-.4,0);
      \coordinate (frd) at (0,0);
      \coordinate (blu) at (-.25,.5);
      \coordinate (bru) at (.15,.5);
      \coordinate (brd) at (.15,.1);
  
      \coordinate (bc) at (-.125,.25);
      \coordinate (bx) at ($(bc)+(1,0)$);
      \coordinate (by) at ($(bc)+(-.6,-.45)$);
      \coordinate (bz) at ($(bc)+(0,-1)$);
  
      \draw[-latex,color=blue!70!black] (bc) -- (bx) node [below,pos=1] {$\vec{x}_b$};
      \draw[-latex,color=blue!70!black] (bc) -- (by) node [left,pos=1] {$\vec{y}_b$};
      \draw[-latex,color=blue!70!black] (bc) -- (bz) node [below,pos=1] {$\vec{z}_b$};
  
      \draw (frd) -- (fru) node [midway,right]  {};
      \draw (frd) -- (brd) node [midway,right]  {};
      \draw (fru) -- (bru) node [midway,right]  {};
      \draw (brd) -- (bru) node [midway,right]  {};
      \draw (flu) -- (fru) node [midway,right]  {};
      \draw (blu) -- (bru) node [midway,right]  {};
      \draw (fld) -- (frd) node [midway,right]  {};
      \draw (fld) -- (flu) node [midway,right]  {};
      \draw (flu) -- (blu) node [midway,right]  {};
  
      \coordinate (a1) at ($(bc)+(4,3)$);
      \coordinate (a2) at ($(bc)+(-1,2)$);
  %    \coordinate (a2) at (3,2);
  
      \node[circle,draw=black,thick] (ca1) at (a1) {};
      \node at (ca1) [above=.2] {GNSS antenna 1};
      \draw[dashed] (bc) -- (a1) node [right=.1,pos=.7] {$L_1$};
  
      \node[circle,draw=black,thick] (ca2) at (a2) {};
      \node at (ca2) [above=.2] {GNSS antenna 2};
      \draw[dashed] (bc) -- (a2) node [left,pos=.5] {$L_2$};
  
      \draw[dashed] (a1) -- (a2) node [above=.1,pos=.5] {$L_{12}$};
  
      \draw[-latex,color=green!50!black] (bc) -- ($(bc)+(.8,.6)$) node [right,pos=1] {$\vec{x}_a$};
  
      \draw[dashed] ($(bc)+(2.08,1.56)$) -- (a2);
  
      \draw[-latex,,color=green!50!black] ($(bc)+(2.08,1.56)$) -- ($(bc)+(1.3,1.67)$) node[above,pos=.5] {$\vec{y}_a$};
  
      \draw[-latex,color=green!50!black] (bc) -- ($(bc)+(-.85,.1)$) node[above,pos=.5] {$\vec{y}_a$};
  
      \draw[-latex,,color=green!50!black] (bc) -- ($(bc)+(.05,.8)$) node[right,pos=1] {$\vec{z}_a$};
      \end{tikzpicture}
      }
  \end{textblock*}
\end{frame}

\begin{frame}
  \frametitle{Attitude representation}
  \vspace{1cm}
  Rotation matrix $R$ can be described by the unit quaternion $q$
  \begin{align*}
    R = I_3 + 2\eta S(\epsilon) + 2S(\epsilon)^2, \;\;\;\;\;\;\;\; 
    q = \begin{bmatrix}
      \eta\\
      \epsilon
    \end{bmatrix}
    \begin{matrix}
      \rightarrow \textnormal{scalar}\\\rightarrow \textnormal{vector}
    \end{matrix}
  \end{align*}
  \pause
  The modified Rodrigues parameter (MRP) $u$ relate to the unit quaternion by
  \begin{align*}
  & u = \frac{4\epsilon}{1+\eta} & q = \frac{1}{\sqrt{16+u^{\top}u}}
                                   \begin{bmatrix}
                                     16 - u^{\top}u\\
                                     8u
                                   \end{bmatrix}&
  \end{align*}
  \pause
  Small angle approximation
  \begin{align*}
    R \approx I_3 + S(u)
  \end{align*}
  
  \begin{textblock*}{5.5cm}(7cm,.2cm)
    \begin{tcolorbox}[colframe=red!80!black]
      \vspace{-15pt}
      \begin{align*}
        S(\epsilon) = \begin{bmatrix}
          0 & -\epsilon_3 & \epsilon_2 \\
          \epsilon_3 & 0 &  -\epsilon_1 \\
          -\epsilon_2 & \epsilon_1 & 0 
        \end{bmatrix}
      \end{align*}
    \end{tcolorbox}
  \end{textblock*}
\end{frame}

\begin{frame}
  \frametitle{Kalman Filter}
  \begin{textblock*}{8cm}(4.5cm,.2cm)
    \onslide<3->{
    \begin{tcolorbox}[colframe=red!80!black]
    \vspace{-10pt}
  \begin{align*}
    \delta p \triangleq p^n_{nb} - \hat{p}^n_{nb} & & \delta u_b \triangleq \frac{4\epsilon^b_{\hat{b}}}{1+\eta^b_{\hat{b}}} & & \delta u_a \triangleq  \frac{4\epsilon^a_{\hat{a}}}{1+\eta^a_{\hat{a}}}
  \end{align*}}
    \vspace{-10pt}
    \end{tcolorbox}
  \end{textblock*}
  \vspace{1cm}
  In the noise-free case
  \begin{align*}
    \delta y_i =& y_i-\hat{y}_i = p^n_{na_i} - \hat{p}^n_{na_i}
\onslide<2->{= (p^n_{nb} + R^n_bR^b_ap^a_{ba_i}) - (\hat{p}^n_{nb} + R^n_{\hat{b}}R^b_{\hat{a}}p^a_{ba_i})}\\
 \onslide<3->{=& \color{red}\delta p\color{black} + R^n_{\hat{b}} \color{red}R^{\hat{b}}_b\color{black} R^b_{\hat{a}} \color{red}R^{\hat{a}}_a\color{black} p^a_{ba_i} - R^n_{\hat{b}}R^b_{\hat{a}}p^a_{ba_i}}\\ 
 \onslide<4->{   =& \color{red}\delta p\color{black} + R^n_{\hat{b}} \color{red}(I_3 - S(\delta u_b))\color{black} R^b_{\hat{a}} \color{red}(I_3 - S (\delta u_a))\color{black} p^a_{ba_i} - R^n_{\hat{b}}R^b_{\hat{a}}p^a_{ba_i}}\\ 
 \onslide<5->{   \approx& \color{red}\delta p\color{black} - R^n_{\hat{b}} \color{red}S(\delta u_b)\color{black} R^b_{\hat{a}}p^a_{ba_i} - R^n_{\hat{b}} R^b_{\hat{a}} \color{red}S(\delta u_a)\color{black} p^a_{ba_i}}\\ 
 \onslide<6->{   \approx& \color{red}\delta p\color{black} + R^n_{\hat{b}} S(R^b_{\hat{a}}p^a_{ba_i}) \color{red}\delta u_b\color{black} + R^n_{\hat{b}} R^b_{\hat{a}} S(p^a_{ba_i}) \color{red}\delta u_a\color{black}}
  \end{align*}
  \begin{minipage}{.4\linewidth}
  \onslide<7->{
  Correction:
 \begin{align*}
    \hat{p}^n_{nb}(+) \leftarrow& \hat{p}^n_{nb}(-) + K_p\delta y\\
    q^n_{\hat{b}} (+) \leftarrow& q^n_{\hat{b}} (-) \otimes q^{\hat{b}}_b\\
    q^b_{\hat{a}} (+) \leftarrow& q^b_{\hat{a}} (-) \otimes q^{\hat{a}}_a
  \end{align*}}
  \end{minipage}
  \begin{textblock*}{7cm}(5.5cm,6.5cm)
    \onslide<7->{
    \begin{tcolorbox}[colframe=red!80!black]
      \vspace{-15pt}
      \begin{align*}
      q^{\hat{a}}_a = \frac{1}{\sqrt{16+\|K_{\delta u_a}\delta y\|^2}}
      \begin{bmatrix}
        16 - \|K_{\delta u_a}\delta y\|^2\\
        8 K_{\delta u_a}\delta y
      \end{bmatrix}
      \end{align*}
    \end{tcolorbox}
    }
  \end{textblock*}
\end{frame}

\begin{frame}
  \frametitle{Observability}
  \begin{itemize}
  \item Hong et al. \footnotemark[1] showed that observability requires non-constant attitude
  \item For $N \geq 2$, IMU and antennas cannot form a line
  \end{itemize}
  
  \footnotetext[1]{\emph{Observability analysis of INS with a GPS multi-antenna system}, Sinpyo Hong et al., KSME International Journal 2002}
\end{frame}

\begin{frame}[t]
  \frametitle{Simulation study}
  Comparing 3 EKF formulations:
  \begin{itemize}
  \item Antenna frame (EKF1)
    \begin{itemize}
    \item $x_{aug} = q^b_a,\;\; \delta x_{aug} = \delta u_a$
    \end{itemize}
  \item Lever arm (EKF2)
    \begin{itemize}
      \item $x_{aug} = [p^b_{ba_1}{}^{\top},\hdots,p^b_{ba_N}{}^{\top}]$
        
            $\delta x_{aug} = [\delta p_{ba_1}{}^{\top},\hdots,\delta p_{ba_N}{}^{\top}]$
      \item Pseudo-measurements
        
        $y_{L_i} = \|p^b_{ba_i}\|_2 + \varepsilon_{L_i}$
        
        $y_{L_{ij}} = \|p^b_{a_ia_j}\|_2+ \varepsilon_{L_{ij}}$
    \end{itemize}
  \item Known lever arms (EKF3)
    \begin{itemize}
    \item $x_{aug} = \delta x_{aug} = []$
    \end{itemize}
  \end{itemize}
  \begin{textblock*}{5.4cm}(7.2cm,2cm)
    \begin{tcolorbox}[colframe=blue!70!black,title=State and error state space]
      \begin{align*}
        x = \begin{bmatrix}
        p^n_{nb}\\
        v^n_{nb}\\
        q^n_b\\
        b^b_{acc}\\
        b^b_{ars}\\
        x_{aug}
        \end{bmatrix},\;\;
        \delta x = \begin{bmatrix}
          \delta p\\
          \delta v\\
          \delta u_b\\
          \delta b_{acc}\\
          \delta b_{ars}\\
          \delta x_{aug}
        \end{bmatrix}
      \end{align*}
    \end{tcolorbox}
  \end{textblock*}
\end{frame}

\begin{frame}[t]
  \frametitle{Simulations study}
  \begin{minipage}{.5\linewidth}
  \begin{itemize}
    \item 50 simulations for $N = 1,2,3$
      \begin{itemize}
      \item $N=1$: two angles
      \end{itemize}
    \item[$\approx$] identical tuning
  \end{itemize}
  \end{minipage}%
  \begin{minipage}{.5\linewidth}
  \end{minipage}
  \begin{textblock*}{12cm}(.5cm,4.5cm)
    \begin{tcolorbox}[colframe=red!80!black]
      \begin{minipage}{.5\linewidth}
        \begin{figure}[t]
          \centering
          \includegraphics[width=\textwidth]{map.eps}
        \end{figure}
      \end{minipage}%
      \begin{minipage}{.5\linewidth}
        \begin{align*}\label{eq:flight-pattern}
          \phi(t) =& \frac{\pi}{12}\sin\frac{2\pi t}{15} + \frac{\pi}{10}\sin\frac{2\pi t}{120}\\
          \theta(t) =& \frac{\pi}{9}\sin\frac{2\pi t}{15},\;
          \dot{\psi}(t) = \frac{\|g^n\|_2}{\|v^n_{nb}\|_2} \phi(t)\\
          v^b_{nb} =&
          \begin{bmatrix}
            30 & 0 &  0
          \end{bmatrix}^{\top} \frac{m}{s}
        \end{align*}
      \end{minipage}
    \end{tcolorbox}
  \end{textblock*}
  \begin{textblock*}{6cm}(7cm,1.5cm)
    $p^b_{ba_1} = [0.5,0,-0.3]m$, $p^b_{ba_2} = [-0.25,0.9,-0.2]m$, $p^b_{ba_3} = [-0.25,-0.9,-0.2]$
  \end{textblock*}
\end{frame}

\begin{frame}
  \frametitle{Results}
  \begin{itemize}
  \item Divergence issues for $N=1$

    \begin{tabular}{|c|c|c|}
      EKF1& EKF2& EKF3\\
      8/50 & 12/50 & 14/50
    \end{tabular}
  \item No divergence for $N=2$ and $N=3$
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{GNSS antenna 1 X-position error}
  \begin{textblock*}{8cm}(4.5cm,1cm)
  \begin{figure}[t]
    \centering
    \includegraphics[width=\linewidth]{../../papers/ecc2019/simplots/GNSS-Antenna-1-Error.pdf} 
  \end{figure}
  \end{textblock*}
  \begin{textblock*}{1.5cm}(2.8cm,2.3cm)
    \Large{$N=1$}
  \end{textblock*}
  \begin{textblock*}{1.5cm}(2.8cm,4.8cm)
    \Large{$N=2$}
  \end{textblock*}
  \begin{textblock*}{1.5cm}(2.8cm,7.3cm)
    \Large{$N=3$}
  \end{textblock*}
\end{frame}

\begin{frame}
  \frametitle{Restults}
  \centering
  %\caption{MAE with one antenna}
  \begin{tabular}{l|c|c|c|c|c|c|}
         Yaw   & \multicolumn{3}{c|}{Transient} & \multicolumn{3}{c|}{Steady State}\\
         $[\textnormal{deg}]$ & EKF1   & EKF2 & EKF3   & EKF1 & EKF2 & EKF3\\\hline
         N=1   &  12.40 & 12.94 & 8.76  & 0.08 & 0.08 & 0.08 \\
         N=2   &  0.42 & 0.44 & 0.21 & 0.10 & 0.09 & 0.12 \\
         N=3   & 0.23 & 0.21 & 0.11 & 0.09 & 0.09 & 0.10\\
  \end{tabular}
  \vspace{.5cm}
  
  \begin{tabular}{l|c|c|c|c|c|c|}
                     & \multicolumn{3}{c|}{Transient} & \multicolumn{3}{c|}{Steady State}\\
                     & EKF1   & EKF2 & EKF3   & EKF1 & EKF2 & EKF3\\\hline
$\|\delta p^b_{ba_1}\|$ [mm] & 210.5  & 161.9 &   -   & 19.8 & 21.1  & - \\\hline
$\|\delta p^b_{ba_1}\|$ [mm] & 27.1 & 24.2 &  -    & 5.5  & 7.2  &  - \\
$\|\delta p^b_{ba_2}\|$ [mm] & 29.0 & 27.0 &  -    & 5.8  & 7.7  &  -   \\\hline
$\|\delta p^b_{ba_1}\|$ [mm] & 5.1  & 4.3  &  -   & 1.6  & 2.0  &  -  \\
$\|\delta p^b_{ba_2}\|$ [mm] & 7.0  & 6.1  &  -   & 2.0  & 2.4  &  -  \\
$\|\delta p^b_{ba_3}\|$ [mm] & 4.7  & 4.8  &  -   & 2.0  & 2.4  &  -  \\
  \end{tabular}
\end{frame}

\begin{frame}
  \frametitle{Conclusions}
  \begin{itemize}
  \item Requiring fewer GNSS antennas than Hong et al.
  \item Incorporates easily acquired distance information without
    pseudo-measurements $\|p^b_{ba_i}\|_2 + \epsilon_{i,pseudo}$
  \end{itemize}
\end{frame}
\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
